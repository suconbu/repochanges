#!/usr/bin/env python3

"""For Git repository manipulation."""

import re
import os
import sys
import subprocess
from urllib.parse import urlparse

class Repository(object):
    __date_format = "--date=format:%Y-%m-%d %H:%M"
    __pretty_format = "--pretty=format:%H | %cd | %an | %s"
    __summary_reobj = re.compile(r"(\w+) \| ([^|]+) \| ([^|]+) \| (.+)")

    def __init__(self, local_path, remote_url=None):
        """
        remote_url is None     -> Use an existing local repository
        remote_url is not None -> Clone/fetch the repository from remote
        """
        path = os.path.abspath(local_path)
        if not remote_url and not Repository.__exists(path):
            raise ValueError("Not found a repository directory - {0}".format(path))

        self.__path = path
        self.__url = None

        if remote_url:
            urlobj = urlparse(remote_url)
            if not urlobj.scheme:
                raise ValueError("{0} is not a URL".format(remote_url))
            self.__url = urlobj.geturl()

    def exists(self):
        return Repository.__exists(self.__path)

    @staticmethod
    def __exists(path):
        return \
            os.path.exists(os.path.join(path, ".git")) or \
            os.path.exists(os.path.join(path, "HEAD"))

    def prepare(self, skip_fetch=False, bare=False):
        """Execute fetch or clone"""
        if self.exists():
            if not skip_fetch or not self.__url:
                self.fetch()
            if not self.__url:
                self.__url = self.__run_git_command("remote", "get-url", "origin")
        else:
            self.clone(bare=bare)

    def fetch(self):
        self.__run_git_command(["fetch"])

    def clone(self, revision=None, bare=False):
        if not self.__url:
            raise ValueError("Remote URL is not specified")
        os.makedirs(self.__path, exist_ok=True)
        command = ["clone", self.__url, self.__path]
        if revision:
            command.append("-b " + revision)
        if bare:
            command.append("--bare")
        self.__run_git_command(command)

    def get_commits(self, revision=None, max_count=None, keyword=None, first_parent=True):
        """Get Commit objects from result of 'git log'

        Args:
            revision  : string -- 1:n (<=n), 2:n.. (n<), 3:n..m (n<.<=m).
            max_count : int    -- Limit the number of commits to result.
            keyword   : string -- Limit the commits output to ones with log message that matches the specified keyword.
        Returns:
            list -- Commit objects (Order by date newer to older)
        """
        command = ["log"]
        if revision:
            command.append(revision)
        if max_count:
            command.append("-n {0}".format(max_count))
        if keyword:
            command.append("--grep={0}".format(keyword))
            command.append("--fixed-strings")
        if first_parent:
            command.append("--first-parent")
            command.append("-c")
        # -z: Separate the commits with NULs instead of with new newlines.
        command.extend([self.__pretty_format, self.__date_format, "--numstat"])

        output = self.__run_git_command(command)
        return self.__create_commits(output)

    def get_filelines(self, revision, file):
        """Returns {list} -- Text lines of file in specific revision"""
        return self.__run_git_command(["show", revision + ":" + file])

    def get_changedlines(self, revision, added=True, deleted=True):
        """Returns {Tuple} -- (added_lines, deleted_lines)"""
        lines = self.__run_git_command(["show", "-m", "-U0", revision])
        if not lines:
            return None
        added_lines = None
        deleted_lines = None
        if added:
            added_lines = [line[1:].strip() for line in lines if line.startswith("+") and not line.startswith("+++")]
        if deleted:
            deleted_lines = [line[1:].strip() for line in lines if line.startswith("-") and not line.startswith("---")]
        return (added_lines, deleted_lines)

    def get_local_path(self):
        return self.__path

    def get_remote_url(self):
        return self.__url

    def __run_git_command(self, command):
        if not os.path.exists(self.__path):
            raise ValueError("Invalid operation")
        return re.split("\r\n|\r|\n", subprocess.check_output(["git"] + command, cwd=self.__path).decode("utf-8"))

    def __create_commits(self, lines):
        """
        Returns {list} -- Commit object list
        Input text format:
        ---
        {sha1} {datetime} {message} [{author-name}]
        {insertions}\t{deletions}\t{filename}
        :
        (Blank line)
        ---
        """
        commits = []
        header_match = None
        changedfiles = []
        for line in lines:
            if not header_match:
                header_match = Repository.__summary_reobj.fullmatch(line)
            else:
                if line:
                    tokens = line.split("\t", maxsplit=2)
                    changedfile = ChangedFile(name=tokens[2], insertions=tokens[0], deletions=tokens[1])
                    changedfiles.append(changedfile)
                else:
                    if header_match:
                        commit = Commit(
                            revision=header_match[1],
                            date=header_match[2],
                            author=header_match[3],
                            message=header_match[4],
                            changedfiles=changedfiles
                        )
                        commits.append(commit)
                    header_match = None
                    changedfiles = []
        return commits

class Commit:
    def __init__(self, revision, date, author, message, changedfiles):
        self.revision = revision
        self.date = date
        self.author = author
        self.message = message
        self.changedfiles = changedfiles
        self.insertions = sum(f.insertions for f in self.changedfiles)
        self.deletions = sum(f.deletions for f in self.changedfiles)

    def __str__(self):
        return "{0} {1} [{2}] ({3}, +{4} / -{5}): {6}".format(
            self.revision[:8], self.date, self.author,
            len(self.changedfiles), self.insertions, self.deletions,
            self.message)

class ChangedFile:
    """Info of one changed file"""
    def __init__(self, name, insertions, deletions):
        self.name = name
        self.insertions = int(insertions) if insertions.isdecimal() else 0
        self.deletions = int(deletions) if deletions.isdecimal() else 0

    def __str__(self):
        return "{0} (+{1} / -{2})".format(self.name, self.insertions, self.deletions)
