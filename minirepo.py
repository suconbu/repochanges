#!/usr/bin/env python3

"""For repo manipulation."""

import re
import os
import sys

class Manifest(object):
    def __init__(self, lines, skip_project=False):
        self.default_revision = None
        self.default_remote = None
        self.remotes = {}
        self.projects = None
        stripped_lines = [line.lstrip() for line in lines]
        for line in stripped_lines:
            if line.startswith("<default"):
                m = re.search(r'revision="([^"]+)"', line)
                self.default_revision = m[1] if m else self.default_revision
                m = re.search(r'remote="([^"]+)"', line)
                self.default_remote = m[1] if m else self.default_remote
            elif line.startswith("<remote"):
                m = re.search(r'name="([^"]+)"', line)
                name = m[1] if m else None
                m = re.search(r'fetch="([^"]+)"', line)
                fetch = m[1] if m else None
                if name and fetch:
                    self.remotes[name] = fetch
        if not skip_project:
            self.projects = []
            for line in stripped_lines:
                project = self.create_project(line)
                if project:
                    self.projects.append(project)

    def create_project(self, line):
        if not line.strip().startswith("<project"):
            return None
        name_match = re.search(r'name="([^"]+)"', line)
        path_match = re.search(r'path="([^"]+)"', line)
        revision_match = re.search(r'revision="([^"]+)"', line)
        remote_match = re.search(r'remote="([^"]+)"', line)

        remote_name = remote_match[1] if remote_match else self.default_remote
        base_url = self.remotes.get(remote_name)
        if base_url and not base_url.endswith("/"):
            base_url += "/"

        name = name_match[1] if name_match else None
        return Project(
            name=name,
            path=path_match[1] if path_match else None,
            revision=revision_match[1] if revision_match else self.default_revision,
            url=(base_url + name) if (base_url and name) else None
        )

class Project(object):
    def __init__(self, name, path, revision, url):
        self.name = name
        self.path = path
        self.revision = revision
        self.url = url
