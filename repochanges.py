#!/usr/bin/env python3

import re
import os
import sys
import json
import minigit
import minirepo
import argparse
import concolor
import traceback
from collections import OrderedDict

_DEBUG = True

def print_exception(e):
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb)

class Styler(object):
    def __init__(self, enable=True):
        self.enable = enable
    
    def get(self, string="", style=""):
        if self.enable:
            bold = style.startswith("*")
            color = style[1:] if bold else style
            return concolor.get(string, color, bold)
        else:
            return string

class SearchCondition:
    def __init__(self, args, setting):
        self.keyword = args.keyword
        self.target = "sha1" if re.fullmatch("[0-9a-f]{8,40}", self.keyword) else "message"
        if args.projectnames:
            self.projectnames = get_projectnames(args.projectnames, setting.project_modifier)
        else:
            self.projectnames = None

class Setting:
    @staticmethod
    def load(path):
        setting = Setting()
        json_root = json.load(open(path), object_pairs_hook=OrderedDict)

        # Require
        setting.working_directory = json_root.get("working-directory", ".")
        setting.default_manifest = json_root.get("default-manifest")

        # Optional
        setting.project_modifier = json_root.get("project-modifier")
        setting.url_modifier = json_root.get("result-url-modifier")

        return setting

class Result:
    def __init__(self):
        self.__commits_by_projectname = {}
        self.__projects_by_projectname = {}

    def add_commits(self, project, commits):
        key = project.name + ":" + project.path
        self.__projects_by_projectname[key] = project
        self.__commits_by_projectname.setdefault(key, [])
        # Excludes duplicate commit
        for commit in commits:
            duplicated = [c for c in self.__commits_by_projectname[key] if c.revision == commit.revision]
            if not duplicated:
                self.__commits_by_projectname[key].append(commit)

    def output(self, styler, searchcond, url_modifier=None, project_modifier=None):
        if styler.enable:
            print()
            print("-" * 80)
            print()

        if not self.__commits_by_projectname:
            print("No matched commits")
            print()
            return

        total_ccount = 0
        total_fcount = 0
        total_icount = 0
        total_dcount = 0
        manifest_name = "manifest"
        for key in self.__commits_by_projectname:
            project = self.__projects_by_projectname[key]
            commits = self.__commits_by_projectname[key]

            plane_project_name = project.name
            if project_modifier:
                m = re.fullmatch(project_modifier.replace("{0}", r"(.+)"), plane_project_name)
                if m:
                    plane_project_name = m.group(1)

            if plane_project_name != manifest_name:
                total_ccount += len(commits)
                count = (
                    sum(len(commit.changedfiles) for commit in commits),
                    sum(commit.insertions for commit in commits),
                    sum(commit.deletions for commit in commits)
                )
                total_fcount += count[0]
                total_icount += count[1]
                total_dcount += count[2]

                print("[{0} ({1}) -- {2} files, {3} / {4} lines]".format(
                    styler.get(plane_project_name, "*"),
                    os.path.split(project.path)[1],
                    count[0],
                    styler.get("+" + str(count[1]), "green"),
                    styler.get("-" + str(count[2]), "red")
                ))
            else:
                print("[{0}]".format(styler.get(plane_project_name, "*")))

            if commits:
                c_len_max = max(len(str(len(commit.changedfiles))) for commit in commits)
                i_len_max = max(len(str(commit.insertions)) for commit in commits)
                d_len_max = max(len(str(commit.deletions)) for commit in commits)
                for commit in commits:
                    url = project.url
                    if url_modifier:
                        url = self.make_url(url, url_modifier, plane_project_name, commit.revision)

                    if plane_project_name != manifest_name:
                        print("{0} {1} ({2}, {3} / {4})".format(
                            styler.get(commit.date, "cyan"),
                            url,
                            str(len(commit.changedfiles)).rjust(c_len_max),
                            styler.get(("+" + str(commit.insertions)).rjust(i_len_max + 1), "green"),
                            styler.get(("-" + str(commit.deletions)).rjust(d_len_max + 1), "red")
                        ))
                    else:
                        print("{0} {1}".format(styler.get(commit.date, "cyan"), url))
            print()

        print("-" * 80)
        print()
        print("{0}: {1} commits, {2} files, {3} / {4} lines".format(
            styler.get("TOTAL", "*"),
            total_ccount,
            total_fcount,
            styler.get("+" + str(total_icount), "green"),
            styler.get("-" + str(total_dcount), "red")
        ))
        print()

    def make_url(self, base_url, url_modifier, project_name, revision):
        m = re.match(r"(?P<scheme>\w+)://(?:\w+@)?(?P<host>[\w.]+)(?::(?P<port>\d+))?", base_url)
        if m:
            scheme = m.group("scheme")
            host = m.group("host")
            port = m.group("port")
        modified_url = url_modifier \
            .replace("{scheme}", scheme or "") \
            .replace("{host}", host or "") \
            .replace("{port}", port or "") \
            .replace("{project}", project_name) \
            .replace("{revision}", revision[:8])
        return modified_url

def get_projects_from_manifest(manifest_repo, searchcond):
    if searchcond.target == "sha1":
        manifest_commits = manifest_repo.get_commits(searchcond.keyword, max_count=1)
    else:
        manifest_commits = manifest_repo.get_commits(keyword=searchcond.keyword)

    projects = []
    for commit in manifest_commits:
        filelines = manifest_repo.get_filelines(commit.revision, "default.xml")
        manifest = minirepo.Manifest(filelines, skip_project=True)
        changedlines = manifest_repo.get_changedlines(commit.revision, deleted=False)
        for line in changedlines[0]:
            project = manifest.create_project(line)
            if project and len([p for p in projects if project.name == p.name and project.revision == p.revision]) == 0:
                projects.append(project)
    return (manifest_commits, projects)

def get_projects_from_projectnames(manifest_repo, projectnames):
    lines = manifest_repo.get_filelines("HEAD", "default.xml")
    manifest = minirepo.Manifest(lines)
    return [p for p in manifest.projects if p.name in projectnames]

def get_projectnames(projectnames, modifier=None):
    """
    'alice,bob,carol,bob' -> ['alice.git', 'bob.git', 'carol.git']
    """
    modifier = modifier if modifier else "{0}"
    unique_names = []
    for name in (modifier.replace("{0}", name) for name in projectnames.split(",")):
        if name not in unique_names:
            unique_names.append(name)
    return unique_names

def add_commits_to_result(working_directory_path, projects, searchcond, result):
    project_repo_cache = {}
    for project in projects:
        project_path = os.path.abspath(os.path.join(working_directory_path, project.path))
        repo = project_repo_cache.get(project.name)
        if not repo:
            repo = minigit.Repository(project_path, project.url)
            repo.prepare(skip_fetch=_DEBUG, bare=True)
            project_repo_cache[project.name] = repo

        if searchcond.projectnames:
            revision = "HEAD"
        else:
            revision = project.revision
        commits = repo.get_commits(revision, keyword=searchcond.keyword)
        result.add_commits(project, commits)

def main(argv):
    app_dir = os.path.dirname(__file__)
    setting = Setting.load(os.path.join(app_dir, "repochanges_setting.json"))
    if not setting:
        print("Cannot open setting file")
        return 1

    ap = argparse.ArgumentParser(add_help=False)
    ap.add_argument("--manifest", dest="manifest_url", required=False, default=setting.default_manifest)
    ap.add_argument("--project", dest="projectnames", required=False, default=None)
    ap.add_argument("--help", action="help")
    ap.add_argument("keyword")
    args = ap.parse_args(args=argv[1:])

    working_directory = os.path.abspath(setting.working_directory.replace("{home}", os.path.expanduser("~")))

    os.makedirs(working_directory, exist_ok=True)

    try:
        searchcond = SearchCondition(args, setting)
        manifest_url = setting.default_manifest
        manifest_path = os.path.join(working_directory, "manifest")
        manifest_repo = minigit.Repository(manifest_path, manifest_url)
        manifest_repo.prepare(skip_fetch=_DEBUG, bare=True)

        result = Result()
        if args.projectnames:
            projects = get_projects_from_projectnames(manifest_repo, searchcond.projectnames)
        else:
            manifeset_commits, projects = get_projects_from_manifest(manifest_repo, searchcond)
            manifest_project = minirepo.Project("manifest", manifest_repo.get_local_path(), None, manifest_repo.get_remote_url())
            result.add_commits(manifest_project, manifeset_commits)
        add_commits_to_result(working_directory, projects, searchcond, result)
        result.output(Styler(sys.stdout.isatty()), searchcond, url_modifier=setting.url_modifier, project_modifier=setting.project_modifier)
    except Exception as e:
        print_exception(e)
        return 1
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
