#!/usr/bin/env python3

import os
import re
import pdb
import minigit
import minirepo
import repochanges

def test_minigit():
    r = minigit.Repository("tests/irozo", "https://gitlab.com/suconbu/irozo.git")
    r.prepare(skip_fetch=True, bare=True)
    changelines = r.get_changedlines("e7ab9ddf")

    add_line_count = len(changelines[0])
    assert(add_line_count == 99)

    del_line_count = len(changelines[1])
    assert(del_line_count == 8)

    commits = r.get_commits(max_count=5)
    assert(len(commits) == 5)

    total_file = 0
    total_add = 0
    total_del = 0
    for c in commits:
        # print(c)
        total_file += len(c.changedfiles)
        for f in c.changedfiles:
            # print("  - {0}".format(f))
            total_add += f.insertions
            total_del += f.deletions
    assert(total_file == 9)
    assert(total_add == 284)
    assert(total_del == 123)

def test_minirepo():
    with open("tests/default.xml", encoding="utf-8") as f:
        lines = f.readlines()

    mani1 = minirepo.Manifest(lines, True)
    assert(mani1.default_remote == "origin")
    assert(mani1.default_revision == "master")
    assert(mani1.remotes["origin"] == "https://gitlab.com/suconbu/")
    assert(mani1.projects == None)

    mani2 = minirepo.Manifest(lines)
    assert(len(mani2.projects) == 3)
    assert(mani2.projects[2].name == "memezo.git")
    assert(mani2.projects[2].path == "lang/memezo")
    assert(mani2.projects[2].revision == "526b75ac651421706939e3f15e8232eaf90329b0")

def test_repochanges_manifest(capfd):
    repochanges.main(["repochanges.py", "U"])
    if not capfd:
        return
    o, e = capfd.readouterr()
    m = re.search(r"TOTAL: (\d+) commits, (\d+) files, \+(\d+) / -(\d+) lines", o)
    assert(m is not None)
    assert(int(m.group(1)) == 3)
    assert(int(m.group(2)) == 19)
    assert(int(m.group(3)) == 506)
    assert(int(m.group(4)) == 31)

def test_repochanges_project(capfd):
    repochanges.main(["repochanges.py", "--project", "miso,memezo", "Add"])
    if not capfd:
        return
    o, e = capfd.readouterr()
    m = re.search(r"TOTAL: (\d+) commits, (\d+) files, \+(\d+) / -(\d+) lines", o)
    assert(m is not None)
    assert(int(m.group(1)) == 46)
    assert(int(m.group(2)) == 164)
    assert(int(m.group(3)) == 3992)
    assert(int(m.group(4)) == 1077)

if __name__ == "__main__":
    test_minigit()
    test_minirepo()
    test_repochanges_manifest(None)
    test_repochanges_project(None)
